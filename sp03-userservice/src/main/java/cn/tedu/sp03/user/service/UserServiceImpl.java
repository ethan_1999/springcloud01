package cn.tedu.sp03.user.service;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.pojo.User;
import cn.tedu.sp01.service.UserService;
import cn.tedu.web.util.JsonUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    //  yml配置的测试用用户数据注入到这个变量
    @Value("${sp.user-service.users}")
    private String userJson;
    @Override
    public User getUser(Integer id) {
        List<User> items = JsonUtil.from(userJson, new TypeReference<List<User>>() {});
        for (User user:items) {
            if (user.getId().equals(id)) {
                return user;
            }
        }

        return new User(id, "name-"+id, "pwd-"+id);
    }
    @Override
    public void addScore(Integer id, Integer score) {
        log.info("增加用户积分，userId="+id+"， score="+score);
    }
}
